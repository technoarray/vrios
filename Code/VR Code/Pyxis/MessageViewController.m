//
//  MessageViewController.m
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "MessageViewController.h"
#import "IAPViewController.h"
#import "SignUpViewController.h"
@interface MessageViewController ()

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(_selectedVideo == nil)
    {
        self.navigationItem.title=@"Subscription Details";
        _message.text=[NSString stringWithFormat:@"Exero Vet is available with a $64.99 yearly and $15.99 monthly subscription recurring, that allows access the ability to view all videos in the database within the mobile application when available.\n\nPayment will be charged to iTunes Account at confirmation of purchase.\nSubscription automatically renews unless auto-renew is turned off at least 24 hours before the end of the current period.\nAccount will be charged for renewal within 24 hours prior to the end of the current period, and identify the cost of the renewal.\nSubscriptions may be managed by the user and auto-renewal may be turned off by going to the user’s Account Settings after purchases.\n\nTerms of use:\n%@\n\nPrivacy Policy:\n%@", @"http://www.pyxisvr.com/terms-of-service", @"http://www.pyxisvr.com/privacy-policy"];
    }
    else
    {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPresented=YES;
    
    _message.text=@"This app uses a lot of data. I understand that using this app while not connected to wifi leaves me subject to the charges and fees of my wireless provider.\n\nThe surgeons in this video are licensed doctors of veterinary medicine. The practise of veterinary medicine is regulated by state & federal laws. Before performing any surgeries ensure that you are in compliance with all laws.\n\nThe procedures demonstrated in this video are an example of one method used by surgeons, and may not be the only method. This video is the property of Pyxis, L.L.C.. Any duplication or sharing of this video, without the written consent of Pyxis L.L.C., is prohibited";
    }
}

#pragma mark -- Button Action

- (IBAction)agree:(id)sender
{
    if(_selectedVideo == nil)
    {
        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"isYearly"]isEqualToString:@"yes"])
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
            {
                IAPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"IAPViewController"];
                controller.isYearly=YES;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"365" forKey:@"totalDays"];
                [userDefaults synchronize];
                [self.navigationController pushViewController:controller animated:YES];
            }
            else
            {
                SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
                controller.isYearly=YES;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"365" forKey:@"totalDays"];
                [userDefaults synchronize];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        else
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
            {
                IAPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"IAPViewController"];
                controller.isYearly=NO;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"30" forKey:@"totalDays"];
                [userDefaults synchronize];
                [self.navigationController pushViewController:controller animated:YES];
            }
            else
            {
                SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
                controller.isYearly=NO;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"30" forKey:@"totalDays"];
                [userDefaults synchronize];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    }
    else
    {
    
    ContinueViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ContinueViewController"];
    controller.selectedVideo=_selectedVideo;
    [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
