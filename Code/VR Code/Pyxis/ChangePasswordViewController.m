//
//  ChangePasswordViewController.m
//  Pyxis
//
//  Created by Saurav on 23/04/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    kumulos = [[Kumulos alloc]init];
    kumulos.delegate=self;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
}

#pragma mark -- Button Action

- (IBAction)changePassword:(id)sender
{
    if (_email.text.length==0)
    {
        [Helper showAlertViewWithTitle:@"Email Address" message:@"Please enter your email address."];
    }
    else if ([Helper validateEmailWithString:_email.text]==NO)
    {
        [Helper showAlertViewWithTitle:@"Alert" message:@"Please enter valid email address."];
    }
    else if (![_email.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]])
    {
        [Helper showAlertViewWithTitle:@"Invalid Email" message:@"Please enter your email address which is associated with this account."];
    }
    else if (_current.text.length==0)
    {
        [Helper showAlertViewWithTitle:@"Current Password" message:@"Please enter your current password."];
    }
    else if (_password.text.length==0)
    {
        [Helper showAlertViewWithTitle:@"New Password" message:@"Please enter your new password."];
    }
    else if (_confirm.text.length==0)
    {
        [Helper showAlertViewWithTitle:@"Confirm Password" message:@"Please confirm your password."];
    }
    else if (![_confirm.text isEqualToString:_password.text])
    {
        [Helper showAlertViewWithTitle:@"Mismatch" message:@"Password did not match."];
    }
    else if ([Helper isInternetConnected]==NO)
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
    else
    {
        [Helper showIndicatorWithText:@"Updating..." inView:self.view];
        [kumulos updatePasswordWithPassword:_current.text andUserID:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue] andNewPassword:_password.text];
    }
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Kumulous Delegate

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation updatePasswordDidCompleteWithResult:(NSNumber *)affectedRows
{
    [Helper hideIndicatorFromView:self.view];
    if ([affectedRows intValue]>0)
    {
        [Helper showAlertViewWithTitle:@"Success" message:@"Password updated successfully."];
        [api updatePassword:_current.text newPassword:_password.text];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [Helper showAlertViewWithTitle:@"Mismatch" message:@"Current password did not match."];
    }
}

#pragma mark -- TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_password becomeFirstResponder];
    }
    else if (textField.tag==2)
    {
        [_confirm becomeFirstResponder];
    }
    else if (textField.tag==4)
    {
        [_current becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
