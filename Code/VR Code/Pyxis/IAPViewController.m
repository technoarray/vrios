//
//  IAPViewController.m
//  Pyxis
//
//  Created by Saurav on 22/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "IAPViewController.h"

@interface IAPViewController ()

@end

@implementation IAPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title=@"Products";
    
    if ([SKPaymentQueue canMakePayments]==YES)
    {
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        if (_isYearly==YES)
        {
            [self validateProductIdentifiers:@[@"pyxis101"]];
        }
        else
        {
            [self validateProductIdentifiers:@[@"exerovet_monthly"]];
        }
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:@"Store is not available at this time on your device. Please try again later."];
        [Helper removeAllKeysFromUserDefaults];
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController setViewControllers:@[controller] animated:YES];
    }
    
    _table.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    kumulos=[[Kumulos alloc]init];
    kumulos.delegate=self;
    
    api=[[WebAPI alloc]init];
    api.delegate=self;
}

#pragma mark -- Helper Methods

- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
{
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                          initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
    
    // Keep a strong reference to the request.
    request = productsRequest;
    productsRequest.delegate = self;
    [productsRequest start];
}

// SKProductsRequestDelegate protocol method
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    [Helper hideIndicatorFromView:self.view];
    products = response.products;
    
    for (NSString *invalidIdentifier in response.invalidProductIdentifiers)
    {
        // Handle any invalid product identifiers.
        NSLog(@"Invalid Identifier: %@", invalidIdentifier);
    }
    
    NSLog(@"%@", products);
    [_table reloadData];
    //[self displayStoreUI]; // Custom method
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return products.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier; //
    UITableViewCell* cell;
    
    CellIdentifier = @"productCell";
    cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    SKProduct *product = [products objectAtIndex:indexPath.row];
    
    UILabel *heading = [cell viewWithTag:1];
    UILabel *description = [cell viewWithTag:2];
    UIButton *purchaseBtn = [cell viewWithTag:3];
    
    heading.text = product.localizedTitle;
    description.text=product.localizedDescription;
    [purchaseBtn addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
    [purchaseBtn setTag:indexPath.row];
    purchaseBtn.layer.cornerRadius=4;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"DVM VR" message:@"Do you want to buy one year subscription of $75?" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *buy = [UIAlertAction actionWithTitle:@"Buy Subscription" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"buy");
        
        SKPayment *payment = [SKPayment paymentWithProduct:[products objectAtIndex:sender.tag]];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        isTransactionInProgress=YES;
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [controller addAction:buy];
    [controller addAction:cancel];
    
    [self presentViewController:controller animated:YES completion:nil];*/
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 101;
}

#pragma mark -- SKPaymentTransactionObserver

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
            {
                NSLog(@"purchased");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                isTransactionInProgress=NO;
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isSubscribed"];
                if (_isYearly==YES)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isYearlyPlan"];
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"isYearlyPlan"];
                }
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                [Helper showAlertViewWithTitle:@"Success" message:@"You have successfully subscribed."];
                [kumulos updateSubscriptionStatusWithIsSubscribed:YES andUserID:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
                [api updateUserSubscriptionStatus:@"1"];
                break;
            }
            case SKPaymentTransactionStateFailed:
            {
                NSString *error = transaction.error.localizedDescription;
                NSLog(@"Transaction error: %@", error);
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                isTransactionInProgress=NO;
                [Helper showAlertViewWithTitle:@"Failed" message:@"Transaction failed"];
                break;
            }
            default:
                NSLog(@"State: %ld", (long)transaction.transactionState);
                break;
        }
    }
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(kumulosProxy *)kumulos apiOperation:(KSAPIOperation *)operation didFailWithError:(NSString *)theError
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation updateSubscriptionStatusDidCompleteWithResult:(NSNumber *)affectedRows
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- Button Action

-(void)purchase:(UIButton *)sender
{
    if (isTransactionInProgress==NO)
    {
        SKPayment *payment = [SKPayment paymentWithProduct:[products objectAtIndex:sender.tag]];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        isTransactionInProgress=YES;
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
