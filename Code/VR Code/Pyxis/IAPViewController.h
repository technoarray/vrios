//
//  IAPViewController.h
//  Pyxis
//
//  Created by Saurav on 22/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "Constants.h"
#import "Helper.h"
#import "Kumulos.h"
#import "WebAPI.h"

@interface IAPViewController : UIViewController<SKProductsRequestDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, SKPaymentTransactionObserver, KumulosDelegate, APIsDelegate>
{
    NSArray *products;
    SKProductsRequest *request;
    BOOL isTransactionInProgress;
    Kumulos *kumulos;
    WebAPI *api;
}

@property BOOL isYearly;
@property (strong, nonatomic) IBOutlet UITableView *table;
@end
