//
//  SignUpViewController.h
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "Kumulos.h"
#import "IAPViewController.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, KumulosDelegate, APIsDelegate>
{
    NSArray *schoolList, *currentStatusList;
    BOOL isChecked;
    NSString *schoolName, *statusName;
    Kumulos *kumulos;
    WebAPI *api;
}

@property BOOL isYearly;

@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *currentStatus;
@property (strong, nonatomic) IBOutlet UITextField *school;
@property (strong, nonatomic) IBOutlet UITextField *confirm;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (strong, nonatomic) IBOutlet UILabel *subscriptionTxt;
@property (strong, nonatomic) IBOutlet UIButton *purchaseBtn;

- (IBAction)back:(id)sender;
- (IBAction)checkBox:(id)sender;
- (IBAction)purchase:(id)sender;
@end
