//
//  SearchViewController.h
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "SearchListViewController.h"

@interface SearchDataViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, APIsDelegate>
{
    NSMutableArray *speciesList, *surgeryList;
    WebAPI *api;
    NSDictionary *selectedSpecies, *selectedSurgery;
}
@property (strong, nonatomic) IBOutlet UIPickerView *speciesPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *surgeryPicker;

@end
