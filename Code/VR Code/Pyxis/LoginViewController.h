//
//  LoginViewController.h
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "Kumulos.h"
#import "WebAPI.h"

@interface LoginViewController : UIViewController<KumulosDelegate, UITextFieldDelegate, UIAlertViewDelegate, APIsDelegate>
{
    Kumulos *kumulos;
    NSDictionary *userDetails;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signUpBtn;
- (IBAction)login:(id)sender;
- (IBAction)signUp:(id)sender;
- (IBAction)forgot:(id)sender;
@end
