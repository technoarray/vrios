//
//  MessageViewController.h
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContinueViewController.h"

@interface MessageViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) NSDictionary *selectedVideo;
@end
