//
//  LoginViewController.m
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden=YES;
    
    [[NSUserDefaults standardUserDefaults] setObject:[Helper getApplicationVersion] forKey:@"version"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    kumulos=[[Kumulos alloc]init];
    kumulos.delegate=self;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
}

#pragma mark -- Button Action

- (IBAction)login:(id)sender
{
    NSString *message=nil;
    if (_email.text.length==0)
    {
        message=@"Please enter your email.";
    }
    else if(![Helper validateEmailWithString:_email.text])
    {
        message=@"Please enter valid email.";
    }
    else if (_password.text.length==0)
    {
        message=@"Please enter password.";
    }
    else if ([Helper isInternetConnected]==NO)
    {
        message=INTERNET_ERROR;
    }
    
    if (message)
    {
        [Helper showAlertViewWithTitle:OOPS message:message];
    }
    else
    {
        [_email resignFirstResponder];
        [_password resignFirstResponder];
        
        [Helper showIndicatorWithText:@"Login..." inView:self.view];
        [api loginUser:_email.text password:_password.text];

        //[kumulos loginUserWithEmailAddress:_email.text andPassword:_password.text];
    }
}

- (IBAction)signUp:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoosePlanViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)forgot:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Forgot Password" message:@"Please enter your email address." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Forgot", nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setPlaceholder:@"Email Address"];
    alert.tag=2;
    [alert show];
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(kumulosProxy *)kumulos apiOperation:(KSAPIOperation *)operation didFailWithError:(NSString *)theError
{
    NSLog(@"%@", theError);
    [Helper hideIndicatorFromView:self.view];
}

-(void)kumulosAPI:(Kumulos *)kumulos1 apiOperation:(KSAPIOperation *)operation loginUserDidCompleteWithResult:(NSArray *)theResults
{
    [Helper hideIndicatorFromView:self.view];
    if (theResults.count>0)
    {
        NSDictionary *dict = [theResults firstObject];
        userDetails = dict;
        NSLog(@"%@", dict);
        
        NSArray *list = [dict objectForKey:@"infos"];
        if (list.count>=2)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Limit Reached" message:@"You can login to maximum of two devices at a time. Login on this device will automatically logout you from other devices. Would you like to continue?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue", nil];
            alert.tag=1;
            [alert show];
        }
        else
        {
            
            //_school.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"school"];
            //_currentStatus.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentStatus"];
            [self moveForward];
        }
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:@"Invalid credentials"];
    }
}

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation saveLoginInfoDidCompleteWithResult:(NSNumber *)newRecordID
{
    if ([newRecordID integerValue]>0)
    {
        
    }
}

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation deleteLoginDetailsDidCompleteWithResult:(NSNumber *)affectedRows
{
    [self moveForward];
}

#pragma mark -- AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            [Helper showIndicatorWithText:@"Loading..." inView:self.view];
            [kumulos deleteLoginDetailsWithUser:[[userDetails objectForKey:@"userID"] integerValue]];
        }
    }
    else if (alertView.tag==2)
    {
        if (buttonIndex==1)
        {
            if ([[[alertView textFieldAtIndex:0] text] length]==0)
            {
                [Helper showAlertViewWithTitle:@"Email Address" message:@"Please enter your email address."];
            }
            else if ([Helper validateEmailWithString:[[alertView textFieldAtIndex:0] text]]==NO)
            {
                [Helper showAlertViewWithTitle:@"Email Address" message:@"Please enter valid email address."];
            }
            else
            {
                [Helper showIndicatorWithText:@"Loading..." inView:self.view];
                [api forgotPassword:[[alertView textFieldAtIndex:0] text]];
            }
        }
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackForgotPasswordSuccess:(NSDictionary *)response
{
    [Helper showAlertViewWithTitle:@"Password Sent" message:@"Your password has been sent to your email address."];
}
-(void)callBackloginSuccess:(NSDictionary *)response
{
    if([[response valueForKey:@"error"]intValue]==1)
    {
        [Helper showAlertViewWithTitle:OOPS message:@"Invalid credentials"];
    }
    else if([[response valueForKey:@"error"]intValue]==0)
    {
       
        userDetails =[response mutableCopy];
        NSLog(@"%@", [[response valueForKey:@"response"]valueForKey:@"school"]);
       
       
      
        //_school.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"school"];
        //_currentStatus.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentStatus"];
        [self moveForward];
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:@"Invalid credentials"];
    }
    
}
#pragma mark -- Helper Methods

-(void)moveForward
{
   
    if ([[[userDetails objectForKey:@"response"] objectForKey:@"subscription"] integerValue]==0)
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoosePlanViewController"];//
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        [Helper saveAllValuesToUserDefault:userDetails];
        [kumulos saveLoginInfoWithDeviceIdentifier:[Helper getIMEINumber] andUser:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
        NSLog(@"%@",[userDetails objectForKey:@"response"]);
        
//        [[NSUserDefaults standardUserDefaults]setValue:[[userDetails valueForKey:@"response"]valueForKey:@"school"] forKey:@"school"];
//        [[NSUserDefaults standardUserDefaults]setValue:[[userDetails valueForKey:@"response"]valueForKey:@"currentStatus"] forKey:@"currentStatus"];
//        [[NSUserDefaults standardUserDefaults]setValue:[[userDetails valueForKey:@"response"]valueForKey:@"uid"] forKey:@"MyId"];
        
        
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark -- UITextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_password becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
