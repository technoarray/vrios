//
//  ContinueViewController.h
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerViewController.h"

@interface ContinueViewController : UIViewController

@property (strong, nonatomic) NSDictionary *selectedVideo;

@end
