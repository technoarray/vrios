//
//  Constants.h
//  browze
//
//  Created by HashBrown Systems on 27/11/14.
//  Copyright (c) 2014 Hashbrown Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

/*********************** BASE URL ****************************/

#define SERVER_URL @"http://52.24.190.134/webservices/index.php"

/*********************** VERSION NUMBER 1 ****************************/

#define USER_VERSION_1 @"v1/user/"

/*********************** COMMON URL ****************************/

//fonts
#define Helvetica_REGULAR @"Helvetica Neue"

//colors
#define APP_COLOR [UIColor colorWithRed:90/255.0f green:160/255.0f blue:219/255.0f alpha:1.0f]

//AlertView Titles
#define ALERT @"Alert"
#define OOPS @"Oops"

// AlertView Error Messages
#define INTERNET_ERROR @"Please check your internet connection!"

//constants
#define OK @"OK"

@interface Constants : NSObject

@end
