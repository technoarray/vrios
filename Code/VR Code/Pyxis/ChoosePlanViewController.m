//
//  ChoosePlanViewController.m
//  Pyxis
//
//  Created by Saurav on 02/03/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import "ChoosePlanViewController.h"
#import "MessageViewController.h"
@interface ChoosePlanViewController ()

@end

@implementation ChoosePlanViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _message.text=@"Save over 65% with a yearly subscription.\nThats a lot of $$.";
}

#pragma mark -- Button Action

- (IBAction)MontlySubscription:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isYearly"];
    UINavigationController *navController = [[UINavigationController alloc]init];
    navController.navigationBarHidden=YES;
    MessageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
    //    NSDictionary *dict=[NSDictionary new];
    //    [dict setValue:@"condition" forKey:@"condition"];
    controller.selectedVideo=nil;
    [navController setViewControllers:@[controller]];
    [self presentViewController:navController animated:YES completion:nil];
    
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
//    {
//        IAPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"IAPViewController"];
//        controller.isYearly=NO;
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@"30" forKey:@"totalDays"];
//        [userDefaults synchronize];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else
//    {
//        SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
//        controller.isYearly=NO;
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@"30" forKey:@"totalDays"];
//        [userDefaults synchronize];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
}
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)YearlySubscription:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"isYearly"];
    UINavigationController *navController = [[UINavigationController alloc]init];
    navController.navigationBarHidden=YES;
    MessageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
//    NSDictionary *dict=[NSDictionary new];
//    [dict setValue:@"condition" forKey:@"condition"];
    controller.selectedVideo=nil;
    [navController setViewControllers:@[controller]];
    [self presentViewController:navController animated:YES completion:nil];
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
//    {
//        IAPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"IAPViewController"];
//        controller.isYearly=YES;
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@"365" forKey:@"totalDays"];
//        [userDefaults synchronize];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else
//    {
//        SignUpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
//        controller.isYearly=YES;
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@"365" forKey:@"totalDays"];
//        [userDefaults synchronize];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
