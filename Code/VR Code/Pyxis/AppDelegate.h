//
//  AppDelegate.h
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "Kumulos.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, KumulosDelegate>
{
    Kumulos *kumulos;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL isPresented;
@property (nonatomic) BOOL shouldRotate;


@end

