//
//  Helper.m
//  browze
//
//  Created by HashBrown Systems on 26/03/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+(BOOL)isLocationServiceEnabled
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

+(void)saveAllValuesToUserDefault:(NSDictionary *)dic
{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%@", [[dic valueForKey:@"response"]valueForKey:@"uid"]] forKey:@"MyId"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"firstName"] forKey:@"firstName"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"lastName"] forKey:@"lastName"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"email"] forKey:@"email"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"currentStatus"] forKey:@"currentStatus"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"subscription"] forKey:@"isSubscribed"];
    [defaults setObject:[[dic valueForKey:@"response"]valueForKey:@"school"] forKey:@"school"];
    
    [defaults synchronize];
}

+(NSNumber *)getNumberFromString:(NSString *)strValue
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    NSNumber *number = [f numberFromString:[NSString stringWithFormat:@"%@", strValue]];
    return number;
}

+(NSString *)getDateStringFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSLog(@"GMT date: %@", dateString);
    return dateString;
}

+(NSString *)getApplicationVersion
{
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return [NSString stringWithFormat:@"%@", appVersionString];
}

+ (NSDate *)getDateFromDateString:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
}

+(NSString *)calculatePlainTextTimeFromSeconds:(float)seconds
{
    NSString *time;
    if (seconds>3599)
    {
        long hours=seconds/3600;
        long remaingMinute=seconds-(hours*3600);
        long minutes=remaingMinute/60;
        long lastSeconds=remaingMinute-(minutes*60);
        
        NSString *hourText=(hours>9?[NSString stringWithFormat:@"%lu", hours]:[NSString stringWithFormat:@"0%lu", hours]);
        
        NSString *minuteText=(minutes>9?[NSString stringWithFormat:@"%lu", minutes]:[NSString stringWithFormat:@"0%lu", minutes]);
        
        NSString *secondsText=(lastSeconds>9?[NSString stringWithFormat:@"%lu", lastSeconds]:[NSString stringWithFormat:@"0%lu", lastSeconds]);
        
        time=[NSString stringWithFormat:@"%@:%@:%@", hourText, minuteText, secondsText];
    }
    else if (seconds>60)
    {
        long minutes=seconds/60;
        long lastSeconds=seconds-(minutes*60);
        
        NSString *minuteText=(minutes>9?[NSString stringWithFormat:@"%lu", minutes]:[NSString stringWithFormat:@"0%lu", minutes]);
        
        NSString *secondsText=(lastSeconds>9?[NSString stringWithFormat:@"%lu", lastSeconds]:[NSString stringWithFormat:@"0%lu", lastSeconds]);
        
        time=[NSString stringWithFormat:@"%@:%@", minuteText, secondsText];
    }
    else
    {
        long sec=seconds;
        NSString *secondsText=(seconds>9?[NSString stringWithFormat:@"%ld", sec]:[NSString stringWithFormat:@"0%ld", sec]);
        
        time=[NSString stringWithFormat:@"00:%@", secondsText];
    }
    return time;
}

+(float)fontSizeForCurrentViewHeight:(float)viewHeight baseHeight:(float)baseHeight baseFontSize:(float)baseFontSize
{
    float heightRatio=viewHeight/baseHeight;
    float fontSize=heightRatio*baseFontSize;
    return fontSize;
}

+(float)calculateViewWidthForView:(float)viewHeight baseWidth:(float)baseWidth baseFontSize:(float)baseFontSize
{
    float heightRatio=viewHeight/baseWidth;
    float fontSize=heightRatio*baseFontSize;
    return fontSize;
}

+(NSString *)getNotificationToken
{
    NSString *hexToken;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    hexToken=[defaults objectForKey:@"token"];
    if (!hexToken)
    {
        hexToken=@"1234567890";
    }
    NSLog(@"Token Data %@",hexToken);
    return hexToken;
}

+(NSString *)getIMEINumber
{
    UIDevice *myDevice = [UIDevice currentDevice];
    NSUUID *identifier = myDevice.identifierForVendor;
    NSString *imei=identifier.UUIDString;
    return imei;
}

+(NSArray *)getSchoolList
{
    NSArray *list = [NSArray arrayWithObjects:@"Auburn University", @"Colorado State University", @"Cornell Universtiy", @"Iowa State University", @"Kansas State University", @"Lincoln Memorial University", @"Louisiana State University", @"Massey University", @"Michigan State University", @"Midwestern University", @"Mississippi State University", @"Murdoch University", @"North Carolina State University", @"Ohio State University", @"Oklahoma State University", @"Oregon State University", @"Purdue University", @"Ross University", @"St Georges University", @"St. Matthew's University", @"State University of Utrecht", @"Texas A&M University", @"The University of Edinburgh", @"The University of Sydney", @"Tufts University", @"Tuskegee University", @"Universidad Nacional Autonoma de MÃ©xico", @"UniversitÃ© de MontrÃ©al", @"University College, Dublin", @"University of Calgery", @"University of California Davis", @"University of Florida", @"University of Georgia", @"University of Glasgow", @"University of Guelph", @"University of Illinois", @"University of London", @"University of Melbourne", @"University of Minnesota", @"University of Missouri", @"University of Nebraska", @"University of Pennsylvania", @"University of Prince Edward Island", @"University of Queensland", @"University of Saskatchewan", @"University of Tennessee", @"University of Wisconsin", @"Virginia-Maryland College of Veterinary Medicine", @"Washington State University", @"Western University of Health Sciences", nil];
    
    list = [list sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return list;
}

+(NSArray *)getCurrentStatusList
{
    return [NSArray arrayWithObjects:@"Current Student", @"Practicing", nil];
}

+(CGRect)calculateHeightForText:(NSString *)text fontName:(NSString *)fontName fontSize:(float)fontSize maximumWidth:(float)width
{
    UIFont *font = [UIFont fontWithName:fontName size:fontSize];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    
    return rect;
}

+(CGRect)calculateHeightForAttributedText:(NSAttributedString *)text font:(UIFont *)font maximumWidth:(float)width
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return rect;
}

+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

+(BOOL)validateEmailWithString:(NSString*)emailmatch
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailmatch rangeOfString:@" "].location==NSNotFound && [emailTest evaluateWithObject:emailmatch])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(BOOL)validateUsernameWithString:(NSString*)username
{
    NSCharacterSet * characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString: username];
    if([[NSCharacterSet alphanumericCharacterSet] isSupersetOfSet: characterSetFromTextField] == NO)
    {
        NSLog( @"there are bogus characters here, throw up a UIAlert at this point");
        return NO;
    }
    else
    {
        return YES;
    }
}

+(BOOL)removeAllKeysFromUserDefaults
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:@"MyId"];
    [defaults removeObjectForKey:@"firstName"];
    [defaults removeObjectForKey:@"lastName"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"referralCode"];
    [defaults removeObjectForKey:@"currentStatus"];
    [defaults removeObjectForKey:@"isSubscribed"];
    [defaults removeObjectForKey:@"school"];
    
    [defaults synchronize];
    
    return YES;
}

#pragma mark -- Internet Connectivity

+(BOOL) isInternetConnected
{
    // called after network status changes
    Reachability* internetReachable = [Reachability reachabilityForInternetConnection];;
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            // NSLog(@"The internet is down.");
            return NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            // NSLog(@"The internet is working via WIFI.");
            return YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            // NSLog(@"The internet is working via WWAN.");
            return YES;
            
            break;
        }
    }
}

#pragma mark -- View Indicator

+(void)showIndicatorWithText:(NSString *)text inView:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = text;
}

+(void)hideIndicatorFromView:(UIView *)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

@end
