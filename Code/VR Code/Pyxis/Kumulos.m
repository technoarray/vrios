//
//  Kumulos.m
//  Kumulos
//
//  Created by Kumulos Bindings Compiler on Apr 23, 2017
//

#import <objc/runtime.h>
#import "Kumulos.h"

@interface KumulosDelegateHolder : NSObject

@property (weak,nonatomic) id <KumulosDelegate> delegate;

@end

@implementation KumulosDelegateHolder

+ (instancetype) createWithDelegate:(id <KumulosDelegate>)delegate {
    KumulosDelegateHolder* holder = [[KumulosDelegateHolder alloc] init];
    holder.delegate = delegate;
    return holder;
}

@end

@implementation Kumulos (Bindings)

- (instancetype) init {
    if (self = [self initWithAPIKey:@"4b85c755-2765-42ed-9627-bab0a33187f8" andSecretKey:@"2tKs3D/Vb4UiSuVd00QPDLvLFOc8a5eLjil2"]) {

    }

    return self;
}

- (void) setDelegate:(id<KumulosDelegate>)delegate {
    KumulosDelegateHolder* holder = [KumulosDelegateHolder createWithDelegate:delegate];
    objc_setAssociatedObject(self, @selector(delegate), holder, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id <KumulosDelegate>) delegate {
    KumulosDelegateHolder* holder = objc_getAssociatedObject(self, @selector(delegate));

    if (nil == holder) {
        return nil;
    }

    return holder.delegate;
}

- (void) invokeDelegateSelector:(SEL)selector withArg:(id)arg andOperation:(KSAPIOperation*)operation {
    id <KumulosDelegate> delegate = self.delegate;

    if (!delegate || ![delegate respondsToSelector:selector]) {
        return;
    }

    NSMethodSignature* signature = [[delegate class] instanceMethodSignatureForSelector:selector];
    NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:signature];

    [invocation setTarget:delegate];
    [invocation setSelector:selector];

    Kumulos* k = self;
    [invocation setArgument:&k atIndex:2];
    [invocation setArgument:&operation atIndex:3];
    [invocation setArgument:&arg atIndex:4];

    [invocation invoke];
}


- (KSAPIOperation*) deleteLoginDetailsWithUser:(NSUInteger)user {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:1];
    params[@"user"] = @(user);

    SEL selector = @selector(kumulosAPI: apiOperation: deleteLoginDetailsDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"deleteLoginDetails" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) deleteUserWithIdentifierWithDeviceIdentifier:(NSString*)deviceIdentifier andUser:(NSUInteger)user {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"deviceIdentifier"] = deviceIdentifier;
    params[@"user"] = @(user);

    SEL selector = @selector(kumulosAPI: apiOperation: deleteUserWithIdentifierDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"deleteUserWithIdentifier" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) getLoginDetailsWithUser:(NSUInteger)user {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:1];
    params[@"user"] = @(user);

    SEL selector = @selector(kumulosAPI: apiOperation: getLoginDetailsDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"getLoginDetails" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) saveLoginInfoWithDeviceIdentifier:(NSString*)deviceIdentifier andUser:(NSUInteger)user {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"deviceIdentifier"] = deviceIdentifier;
    params[@"user"] = @(user);

    SEL selector = @selector(kumulosAPI: apiOperation: saveLoginInfoDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"saveLoginInfo" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) checkEmailExistWithEmailAddress:(NSString*)emailAddress {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:1];
    params[@"emailAddress"] = emailAddress;

    SEL selector = @selector(kumulosAPI: apiOperation: checkEmailExistDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"checkEmailExist" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) loginUserWithEmailAddress:(NSString*)emailAddress andPassword:(NSString*)password {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"emailAddress"] = emailAddress;
    params[@"password"] = password;

    SEL selector = @selector(kumulosAPI: apiOperation: loginUserDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"loginUser" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) signUpWithFirstName:(NSString*)firstName andLastName:(NSString*)lastName andEmailAddress:(NSString*)emailAddress andPassword:(NSString*)password andIsSubscribed:(BOOL)isSubscribed andCurrentStatus:(NSString*)currentStatus andSchool:(NSString*)school {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:7];
    params[@"firstName"] = firstName;
    params[@"lastName"] = lastName;
    params[@"emailAddress"] = emailAddress;
    params[@"password"] = password;
    params[@"isSubscribed"] = @(isSubscribed);
    params[@"currentStatus"] = currentStatus;
    params[@"school"] = school;

    SEL selector = @selector(kumulosAPI: apiOperation: signUpDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"signUp" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) updateCurrentStatusWithCurrentStatus:(NSString*)currentStatus andUserID:(NSUInteger)userID {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"currentStatus"] = currentStatus;
    params[@"userID"] = @(userID);

    SEL selector = @selector(kumulosAPI: apiOperation: updateCurrentStatusDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"updateCurrentStatus" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) updatePasswordWithPassword:(NSString*)password andUserID:(NSUInteger)userID andNewPassword:(NSString*)newPassword {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:3];
    params[@"password"] = password;
    params[@"userID"] = @(userID);
    params[@"newPassword"] = newPassword;

    SEL selector = @selector(kumulosAPI: apiOperation: updatePasswordDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"updatePassword" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) updateSchoolWithSchool:(NSString*)school andUserID:(NSUInteger)userID {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"school"] = school;
    params[@"userID"] = @(userID);

    SEL selector = @selector(kumulosAPI: apiOperation: updateSchoolDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"updateSchool" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) updateSubscriptionStatusWithIsSubscribed:(BOOL)isSubscribed andUserID:(NSUInteger)userID {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"isSubscribed"] = @(isSubscribed);
    params[@"userID"] = @(userID);

    SEL selector = @selector(kumulosAPI: apiOperation: updateSubscriptionStatusDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"updateSubscriptionStatus" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

- (KSAPIOperation*) userDetailWithUserID:(NSUInteger)userID {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:1];
    params[@"userID"] = @(userID);

    SEL selector = @selector(kumulosAPI: apiOperation: userDetailDidCompleteWithResult:);

    KSAPIOperation* op = [self callMethod:@"userDetail" withParams:params success:^(KSAPIResponse* response, KSAPIOperation* operation) {
        [self invokeDelegateSelector:selector withArg:response.payload andOperation:operation];
    } andFailure:^(NSError* error, KSAPIOperation* operation) {
        SEL selector = @selector(kumulosAPI:apiOperation:didFailWithError:);
        [self invokeDelegateSelector:selector withArg:[error description] andOperation:operation];
    }];

    return op;
}

@end
