//
//  SettingsViewController.h
//  Pyxis
//
//  Created by Saurav on 21/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "Kumulos.h"
#import "SearchDataViewController.h"

@interface SettingsViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate, KumulosDelegate, APIsDelegate>
{
    IBOutlet UIButton *subscriptionButton;
    UIView *pickerParentView;
    Kumulos *kumulos;
    WebAPI *api;
    NSArray *schoolList, *statusList;
    NSString *schoolName, *status;
}

@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *school;
@property (strong, nonatomic) IBOutlet UILabel *currentStatus;
@property (strong, nonatomic) IBOutlet UILabel *version;
@property (strong, nonatomic) IBOutlet UILabel *plan;

- (IBAction)account:(id)sender;
- (IBAction)changeView:(id)sender;
- (IBAction)updateSchool:(id)sender;
- (IBAction)updateStatus:(id)sender;
- (IBAction)rateApp:(id)sender;
- (IBAction)productSafety:(id)sender;
- (IBAction)search:(id)sender;
- (IBAction)howToUse:(id)sender;
- (IBAction)requestSurgery:(id)sender;
@end
