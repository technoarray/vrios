//
//  TermsViewController.m
//  Pyxis
//
//  Created by Saurav on 18/01/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title=@"Subscription Details";
    _termsTxt.text=[NSString stringWithFormat:@"Exero Vet is available with a $64.99 yearly and $15.99 monthly subscription recurring, that allows access the ability to view all videos in the database within the mobile application when available.\n\nPayment will be charged to iTunes Account at confirmation of purchase.\nSubscription automatically renews unless auto-renew is turned off at least 24 hours before the end of the current period.\nAccount will be charged for renewal within 24 hours prior to the end of the current period, and identify the cost of the renewal.\nSubscriptions may be managed by the user and auto-renewal may be turned off by going to the user’s Account Settings after purchases.\n\nTerms of use:\n%@\n\nPrivacy Policy:\n%@", @"http://www.pyxisvr.com/terms-of-service", @"http://www.pyxisvr.com/privacy-policy"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)agree:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Agree" object:nil];
    }];
}

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
