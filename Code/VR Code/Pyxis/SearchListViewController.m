//
//  SearchListViewController.m
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "SearchListViewController.h"

@interface SearchListViewController ()

@end

@implementation SearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    api=[[WebAPI alloc]init];
    api.delegate=self;
    
    _table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _table.backgroundColor=[UIColor clearColor];
    
    if ([Helper isInternetConnected]==YES)
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getVideosList:_selectedSurgery];
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackVideoListSuccess:(NSDictionary *)response
{
    videosList = [response objectForKey:@"videosList"];
    [_table reloadData];
}

#pragma mark -- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return videosList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videoCell"];
    
    NSDictionary *dict = [videosList objectAtIndex:indexPath.row];
    
    UILabel *title = [cell viewWithTag:1];
    title.text = [dict objectForKey:@"title"];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController *navController = [[UINavigationController alloc]init];
    navController.navigationBarHidden=YES;
    MessageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
    controller.selectedVideo=[videosList objectAtIndex:indexPath.row];
    [navController setViewControllers:@[controller]];
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
