//
//  SearchViewController.m
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "SearchDataViewController.h"

@interface SearchDataViewController ()

@end

@implementation SearchDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    surgeryList=[[NSMutableArray alloc]init];
    speciesList=[[NSMutableArray alloc]init];
    api=[[WebAPI alloc]init];
    api.delegate=self;
    
    if ([Helper isInternetConnected]==YES)
    {
        [Helper showIndicatorWithText:@"Loading..." inView:self.view];
        [api getCategoriesList];
    }
    else
    {
        [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackCategoryListSuccess:(NSDictionary *)response
{
    NSArray *list = [response objectForKey:@"categoriesList"];
    for (NSDictionary *dict in list)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"speciesid==%d", [[dict objectForKey:@"speciesid"] integerValue]];
        NSArray *existList = [list filteredArrayUsingPredicate:predicate];
        if (existList.count==0)
        {
            [speciesList addObject:dict];
        }
    }
    
    if (speciesList.count>0)
    {
        selectedSpecies=[speciesList firstObject];
        surgeryList = [[selectedSpecies objectForKey:@"subCategories"] mutableCopy];
        selectedSurgery=[surgeryList firstObject];
    }
    [_speciesPicker reloadComponent:0];
    [_surgeryPicker reloadComponent:0];
}

#pragma mark -- UIPickerView Delegate

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        return surgeryList.count;
    }
    return speciesList.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        NSDictionary *dict = surgeryList[row];
        return [dict objectForKey:@"surgeryName"];
    }
    NSDictionary *dict = speciesList[row];
    return [dict objectForKey:@"speciesName"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    if (pickerView.tag==2)
    {
        selectedSurgery = surgeryList[row];
        return;
    }
    selectedSpecies = speciesList[row];
    surgeryList = [[selectedSpecies objectForKey:@"subCategories"] mutableCopy];
    selectedSurgery=[surgeryList firstObject];
    [_surgeryPicker reloadComponent:0];
}

#pragma mark -- Button Action

- (IBAction)searchList:(id)sender
{
    if (selectedSurgery)
    {
        SearchListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchListViewController"];
        controller.selectedSurgery=selectedSurgery;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
