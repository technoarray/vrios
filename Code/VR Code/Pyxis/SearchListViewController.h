//
//  SearchListViewController.h
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "WebAPI.h"
#import "MessageViewController.h"

@interface SearchListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, APIsDelegate>
{
    NSArray *videosList;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSDictionary *selectedSurgery;

@end
