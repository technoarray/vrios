//
//  TermsViewController.h
//  Pyxis
//
//  Created by Saurav on 18/01/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *termsTxt;
- (IBAction)agree:(id)sender;
- (IBAction)cancel:(id)sender;
@end
