//
//  WebAPI.m
//  browze
//
//  Created by HashBrown Systems on 04/05/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import "WebAPI.h"

@implementation WebAPI
@synthesize delegate;

#pragma mark -- Authentication Methods

-(void)getAppInfo
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"aboutus" forKey:@"method"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    [[self delegate] callBackAppInfoSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getCategoriesList
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"getCategories" forKey:@"method"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    [[self delegate] callBackCategoryListSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)signupUserWithDetails:(NSDictionary *)details
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"signup" forKey:@"method"];
    [dict setObject:[details objectForKey:@"userID"] forKey:@"userid"];
    [dict setObject:[details objectForKey:@"firstName"] forKey:@"firstName"];
    [dict setObject:[details objectForKey:@"lastName"] forKey:@"lastName"];
    [dict setObject:[details objectForKey:@"emailAddress"] forKey:@"email"];
    [dict setObject:[details objectForKey:@"currentStatus"] forKey:@"currentStatus"];
    [dict setObject:[details objectForKey:@"school"] forKey:@"school"];
    [dict setObject:[details objectForKey:@"isSubscribed"] forKey:@"subscription"];
    [dict setObject:[details objectForKey:@"password"] forKey:@"password"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    [[self delegate] callBackSignupSuccess];
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)getVideosList:(NSDictionary *)details
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"videoslinkList" forKey:@"method"];
    [dict setObject:[details objectForKey:@"speciesid"] forKey:@"speciesid"];
    [dict setObject:[details objectForKey:@"surgeryid"] forKey:@"surgeryid"];
    [dict setObject:@"1" forKey:@"all"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    [[self delegate] callBackVideoListSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)updateUserSubscriptionStatus:(NSString *)status
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"subscriptionUpdate" forKey:@"method"];
    [dict setObject:status forKey:@"subscription"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"totalDays"] forKey:@"subscription_duration"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"userid"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                //[Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            //[Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)updatePassword:(NSString *)currentPassword newPassword:(NSString *)newPassword
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"changePassword" forKey:@"method"];
    [dict setObject:currentPassword forKey:@"opassword"];
    [dict setObject:newPassword forKey:@"password"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"userid"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                //[Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            //[Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            //[[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)updateUserProfileWithType:(NSString *)type value:(NSString *)value
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"updateprofile" forKey:@"method"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] forKey:@"userid"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:value forKey:@"fieldtext"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    
                }
                else
                {
                    //[Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                //[Helper showAlertViewWithTitle:ALERT message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            //[Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            //[[self delegate] callbackFromAPI:nil];
        }
    }];
}

-(void)forgotPassword:(NSString *)email
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"forgotPassword" forKey:@"method"];
    [dict setObject:email forKey:@"email"];
    
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"success"] integerValue]==1)
                {
                    [[self delegate] callBackForgotPasswordSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}
-(void)loginUser:(NSString *)email password:(NSString *)pass;
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"login" forKey:@"method"];
    [dict setObject:email forKey:@"email"];
    [dict setObject:pass forKey:@"password"];
    [[ConnectionHandler getSharedInstance] jsonPostDataHTTP:@"" :dict onCompletion:^(id completed) {
        @try {
            if (![completed isKindOfClass:[NSError class]])
            {
                if ([[completed objectForKey:@"error"] integerValue]==0)
                {
                    [[self delegate] callBackloginSuccess:completed];
                }
                else if ([[completed objectForKey:@"error"] integerValue]==1)
                {
                    [[self delegate] callBackloginSuccess:completed];
                }
                else
                {
                    [Helper showAlertViewWithTitle:ALERT message:[completed objectForKey:@"message"]];
                }
            }
            else
            {
                [Helper showAlertViewWithTitle:OOPS message:INTERNET_ERROR];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
            [Helper showAlertViewWithTitle:ALERT message:[exception description]];
        }
        @finally {
            [[self delegate] callbackFromAPI:nil];
        }
    }];
}

@end
