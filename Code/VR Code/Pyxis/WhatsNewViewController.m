//
//  WhatsNewViewController.m
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "WhatsNewViewController.h"

@interface WhatsNewViewController ()

@end

@implementation WhatsNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    api=[[WebAPI alloc]init];
    api.delegate=self;
    
    [Helper showIndicatorWithText:@"Loading..." inView:self.view];
    [api getAppInfo];
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackAppInfoSuccess:(NSDictionary *)response
{
    NSArray *list = [response objectForKey:@"aboutUs"];
    NSDictionary *dict = [list firstObject];
    _whatsNewTxt.text=[dict objectForKey:@"whatsNew"];
    _aboutUsTxt.text=[dict objectForKey:@"aboutUs"];
    //_whatsNewTxt.text=@"V.0.0.1\n- Recently added: LSU Round #1";
    //_aboutUsTxt.text=@"V.0.0.1\n- We make VR fun!";
}

#pragma mark -- Button Action

- (IBAction)login:(id)sender
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"version"])
    {
        if ([[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"version"]] isEqualToString:[Helper getApplicationVersion]])
        {
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"MyId"]);
            NSString *at=[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"];

            if (at == (id)[NSNull null]|| at.length == 0  || [at isEqual:@"(null)"])
            {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isSubscribed"] integerValue]==0)
                {
                    [Helper removeAllKeysFromUserDefaults];
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    self.navigationController.navigationBarHidden=YES;
                    [self.navigationController setViewControllers:@[controller]];
                }
                else
                {
                    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    self.navigationController.navigationBarHidden=YES;
                    [self.navigationController setViewControllers:@[controller]];
                }

            }
            else
            {
                UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
                self.navigationController.navigationBarHidden=YES;
                [self.navigationController setViewControllers:@[controller]];
               
            }
        }
        else
        {
            UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            self.navigationController.navigationBarHidden=YES;
            [self.navigationController setViewControllers:@[controller]];
        }
    }
    else
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        self.navigationController.navigationBarHidden=YES;
        [self.navigationController setViewControllers:@[controller]];
    }

}

- (IBAction)videoList:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.pyxisvr.com/exero-vet-database-list"]];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
