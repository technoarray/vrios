//
//  AppDelegate.m
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "AppDelegate.h"
  @interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[JWPlayerController setPlayerKey:@"lCrQAkxSLXd+tzfln16osm/LpJroKHMWzlUJF2M4N24="];
    // Override point for customization after application launch.
     _isPresented=NO;
    
    kumulos = [[Kumulos alloc]init];
    kumulos.delegate=self;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"version"])
//    {
//        if ([[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"version"]] isEqualToString:[Helper getApplicationVersion]])
//        {
//            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"]);
//            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
//            {
//                UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
//                navController.navigationBarHidden=YES;
//                [navController setViewControllers:@[controller]];
//            }
//            else
//            {
//                UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//                navController.navigationBarHidden=YES;
//                [navController setViewControllers:@[controller]];
//            }
//        }
//    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"])
    {
        [kumulos getLoginDetailsWithUser:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([self.window.rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        if (_isPresented)  //Check current controller state
        {
            return UIInterfaceOrientationMaskLandscapeRight;
        }
        
        else return UIInterfaceOrientationMaskPortrait;
    }
    else return UIInterfaceOrientationMaskPortrait;
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation getLoginDetailsDidCompleteWithResult:(NSArray *)theResults
{
    BOOL isUserExist=NO;
    for (NSDictionary *dict in theResults)
    {
        if ([[dict objectForKey:@"deviceIdentifier"] isEqualToString:[Helper getIMEINumber]])
        {
            isUserExist=YES;
            break;
        }
    }
    
    if (isUserExist==NO)
    {
        [Helper showAlertViewWithTitle:@"Logged out" message:@"You have logged in on another two devices. You have automatically beed logged out from this device."];
        UINavigationController *navController = (UINavigationController *) self.window.rootViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [navController setViewControllers:@[controller]];
    }
}
 
@end
