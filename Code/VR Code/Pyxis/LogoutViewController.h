//
//  LogoutViewController.h
//  Pyxis
//
//  Created by Saurav on 21/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "Kumulos.h"

@interface LogoutViewController : UIViewController<KumulosDelegate>
{
    Kumulos *kumulos;
}

@property (strong, nonatomic) IBOutlet UIButton *changePassword;
@property (strong, nonatomic) IBOutlet UIButton *signOutBtn;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)back:(id)sender;
- (IBAction)signOut:(id)sender;
- (IBAction)changePassword:(id)sender;
@end
