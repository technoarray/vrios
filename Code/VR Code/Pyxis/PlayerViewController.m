//
//  PlayerViewController.m
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "PlayerViewController.h"
@interface PlayerViewController ()

@end

@implementation PlayerViewController

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden=TRUE;
     [_indicator startAnimating];

    isPlaying=NO;
    _playPauseBtn.hidden=YES;
    _seekBarView.hidden=YES;
    
    //NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"video1" ofType:@"mp4"];
    //[_videoView loadFromUrl:[[NSURL alloc] initFileURLWithPath:videoPath]];
    
   // NSString *url = [NSString stringWithFormat:@"https://s3.amazonaws.com/dev.pyxis.co.in/%@", [_selectedVideo objectForKey:@"video"]];
    NSString *url = [NSString stringWithFormat:@"%@", [_selectedVideo objectForKey:@"video"]];
    
    //[_videoView loadFromUrl:[NSURL URLWithString:url]];
    
    /*CGRect initialFrame;
    
    if ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeRight || [[UIDevice currentDevice] orientation] == UIInterfaceOrientationLandscapeLeft)
    {
        initialFrame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else
    {
        initialFrame=CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width);
    }*/
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    
    _player = [[JWVrVideoView alloc] init];
    [self.view addSubview:_player];
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:_player
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:_player
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:_player
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:self.view
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                 constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:_player
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.view
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1
                                  constant:0];
    
    [NSLayoutConstraint activateConstraints:@[trailing, leading, bottom, height]];
    
    //Add constraints to the Parent
    /*[parent addConstraint:trailing];
    [parent addConstraint:bottom];
    [parent addConstraint:leading];
    
    //Add height constraint to the subview, as subview owns it.
    [subView addConstraint:height];*/
    
    //self.view.translatesAutoresizingMaskIntoConstraints=NO;
    _player.translatesAutoresizingMaskIntoConstraints=NO;
    
    _player.delegate=self;
    [_player load:url withStereoMode:STEREO_MODE_MONO];
    [_player play];
    
    UIButton * backBUTTON=[UIButton buttonWithType:UIButtonTypeCustom];
    backBUTTON.frame=CGRectMake(15, 20, 40, 40);
    [backBUTTON setImage:[UIImage imageNamed:@"back_white"]  forState:UIControlStateNormal];
    [backBUTTON addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBUTTON];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPresented=YES;
}

-(void)backButtonPressed:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPresented=NO;
    
}

#pragma mark -- Button Action

- (IBAction)PlayPause:(id)sender
{
    if (isPlaying==YES)
    {
        isPlaying=NO;
        [_playPauseBtn setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
     }
    else
    {
        isPlaying=YES;
        [_playPauseBtn setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
     }
}

- (IBAction)sliderPositionChanged:(id)sender
{
    playingTime=_slider.value;
   // [_videoView seekTo:_slider.value];
    [self updatePlayingTimeText];
}

- (IBAction)back:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- Helper Method

-(void)updateTotalTimeText
{
    float hours = floor(lroundf(totalDuration)/(60*60));
    float minutes = floor((lroundf(totalDuration) - (hours * 60 * 60))/60);
    float seconds = lroundf(totalDuration) - (hours * 60 * 60) - (minutes * 60);
    if (seconds<0)
    {
        seconds=0;
    }
    
    int roundedHours = (int)lroundf(hours);
    int roundedSeconds = (int)lroundf(seconds);
    int roundedMinutes = (int)lroundf(minutes);
    
    NSString *time;
    if (roundedHours==0)
    {
        time = [[NSString alloc]
                          initWithFormat:@"%02d:%02d",
                          roundedMinutes, roundedSeconds];
    }
    else
    {
        time = [[NSString alloc]
                          initWithFormat:@"%02d:%02d:%02d", roundedHours,
                          roundedMinutes, roundedSeconds];
    }
    _totalTime.text = time;
}

-(void)updatePlayingTimeText
{
    float hours = floor(lroundf(playingTime)/(60*60));
    float minutes = floor((lroundf(playingTime) - (hours * 60 * 60))/60);
    float seconds = lroundf(playingTime) - (hours * 60 * 60) - (minutes * 60);
    if (seconds<0)
    {
        seconds=0;
    }
    int roundedHours = (int)lroundf(hours);
    int roundedSeconds = (int)lroundf(seconds);
    int roundedMinutes = (int)lroundf(minutes);
    
    NSString *time;
    if (roundedHours==0)
    {
        time = [[NSString alloc]
                initWithFormat:@"%02d:%02d",
                roundedMinutes, roundedSeconds];
    }
    else
    {
        time = [[NSString alloc]
                initWithFormat:@"%02d:%02d:%02d", roundedHours,
                roundedMinutes, roundedSeconds];
    }
    _startTime.text = time;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - JWVrVideoViewDelegate overrides
- (void)onPlay {
    NSLog(@"onPlay, state: %@", _player.playerState);
}
- (void)onBuffer {
    NSLog(@"onBuffer, state: %@", _player.playerState);
}
- (void)onPause {
    NSLog(@"onPause, state: %@", _player.playerState);
}
- (void)onComplete {
    NSLog(@"onComplete, state: %@", _player.playerState);
}
- (void)onFullscreenToggled:(BOOL)fullscreen {
    NSLog(@"onFullscreenToggled");
}
- (void)onVRModeEnabled:(BOOL)enabled {
    NSLog(@"Setting vr mode enabled to: %@", enabled ? @"TRUE" : @"FALSE");
}
- (void)onCardboardTrigger {
    NSLog(@"onCardboardTrigger");
    if ([_player.playerState isEqualToString:STATE_PLAYING]) {
        [_player pause];
    } else if (![_player.playerState isEqualToString:STATE_COMPLETE]){
        [_player play];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
