//
//  SettingsViewController.m
//  Pyxis
//
//  Created by Saurav on 21/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

-(void)viewWillAppear:(BOOL)animated
{
    _school.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"school"];
    _currentStatus.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentStatus"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [subscriptionButton addTarget:self
               action:@selector(subscriptionDetail:)
     forControlEvents:UIControlEventTouchUpInside];
    [subscriptionButton setTitle:@"" forState:UIControlStateNormal];
    subscriptionButton.frame = CGRectMake(_plan.frame.origin.x, _plan.frame.origin.y, _plan.frame.size.width, _plan.frame.size.height);
    
    
    self.navigationController.navigationBarHidden=YES;
    
    _email.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isYearlyPlan"])
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isYearlyPlan"] integerValue]==0)
        {
            _plan.text=@"Monthly";
        }
    }
    
    kumulos = [[Kumulos alloc]init];
    kumulos.delegate=self;
    
    api = [[WebAPI alloc]init];
    api.delegate=self;
    
    _version.text=[Helper getApplicationVersion];
    
    schoolList=[Helper getSchoolList];
    
    statusList = [Helper getCurrentStatusList];
}

#pragma mark -- Button Action

- (IBAction)account:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LogoutViewController"];
    [self.navigationController pushViewController:controller animated:YES];//LogoutViewController
}

- (IBAction)changeView:(id)sender
{
    
}
- (void)subscriptionDetail:(id)sender
{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Some parts of the Service are billed on a subscription basis (Subscription(s)). If you select the per year subscription option you will be billed $64.99 in advance on a recurring and periodic basis (Billing Cycle). If you select the per month subscription option you will be billed $15.99 in advance on a recurring and periodic basis (Billing Cycle). Billing cycles are set on a annual and monthly basis.                               At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless you cancel it or Pyxis, L.L.C. cancels it. You may cancel your Subscription renewal either through your online account management page or by contacting Pyxis, L.L.C. customer support team "preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)updateSchool:(id)sender
{
    [self showPickerViewForTag:1];
}

- (IBAction)updateStatus:(id)sender
{
    [self showPickerViewForTag:2];
}

- (IBAction)rateApp:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/pyxisapp/id1189021972?ls=1&mt=8"]];
}

- (IBAction)productSafety:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://vr.google.com/cardboard/product-safety/"]];
}

- (IBAction)search:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDataViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)howToUse:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.pyxisvr.com/exero-vet"]];
}

- (IBAction)requestSurgery:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        NSArray *recipients = [NSArray arrayWithObject:@"request@pyxisvr.com"];
        [picker setToRecipients:recipients];
        [picker setSubject:@""];
        [picker setMessageBody:@"" isHTML:NO];
        [self presentViewController:picker animated:YES completion:nil];
        if (UIUserInterfaceIdiomPhone==UI_USER_INTERFACE_IDIOM())
        {
            
        }
        else
        {
            
        }
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"No Email" message:@"No mail account setup on your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(void)updateClicked
{
    _school.text=schoolName;
    
    [[NSUserDefaults standardUserDefaults]setValue:schoolName forKey:@"school"];
    [pickerParentView removeFromSuperview];
    [kumulos updateSchoolWithSchool:schoolName andUserID:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
}

-(void)updateCurrentStatus
{
    _currentStatus.text=status;
    [pickerParentView removeFromSuperview];
    
    [[NSUserDefaults standardUserDefaults]setValue:status forKey:@"currentStatus"];
    [kumulos updateCurrentStatusWithCurrentStatus:status andUserID:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
}

-(void)cancel
{
    [pickerParentView removeFromSuperview];
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation updateCurrentStatusDidCompleteWithResult:(NSNumber *)affectedRows
{
    [[NSUserDefaults standardUserDefaults] setObject:_currentStatus.text forKey:@"currentStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [api updateUserProfileWithType:@"1" value:status];
}

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation updateSchoolDidCompleteWithResult:(NSNumber *)affectedRows
{
    [[NSUserDefaults standardUserDefaults] setObject:_school.text forKey:@"school"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [api updateUserProfileWithType:@"2" value:schoolName];
}

#pragma mark -- Helper Method

-(void)showPickerViewForTag:(int)tag
{
    pickerParentView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-244, self.view.frame.size.width, 244)];
    pickerParentView.backgroundColor=[UIColor whiteColor];
    
    UIPickerView *picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 200)];
    picker.delegate=self;
    picker.dataSource=self;
    picker.showsSelectionIndicator=YES;
    picker.tag=tag;
    picker.backgroundColor=[UIColor whiteColor];
    
    UIToolbar* toolbar = [[UIToolbar alloc] init];
    toolbar.frame=CGRectMake(0,0,self.view.frame.size.width,44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton;
    if (tag==1)
    {
        schoolName = [schoolList firstObject];
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Update"
                                                      style:UIBarButtonItemStyleDone target:self
                                                     action:@selector(updateClicked)];
    }
    else
    {
        status=[statusList firstObject];
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Update"
                                                      style:UIBarButtonItemStyleDone target:self
                                                     action:@selector(updateCurrentStatus)];
    }
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                  style:UIBarButtonItemStylePlain target:self
                                                                 action:@selector(cancel)];
    
    
    
    [toolbar setItems:[NSArray arrayWithObjects:cancelBtn, flexibleSpaceLeft, doneButton, nil]];
    
    [pickerParentView addSubview:toolbar];
    [pickerParentView addSubview:picker];
    [self.view addSubview:pickerParentView];
}

#pragma mark -- UIPickerView Delegate

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        return statusList.count;
    }
    return schoolList.count;
}

// The data to return for the row and component (column) that's being passed in
/*- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        return statusList[row];
    }
    return schoolList[row];
}*/

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        tView.textAlignment=NSTextAlignmentCenter;
        [tView setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        //[tView setTextAlignment:UITextAlignmentLeft];
        tView.numberOfLines=1;
    }
    // Fill the label text here
    if (pickerView.tag==2)
    {
        tView.text = statusList[row];
    }
    else
    {
        tView.text = schoolList[row];
    }
    return tView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    if (pickerView.tag==2)
    {
        status = statusList[row];
        return;
    }
    schoolName=schoolList[row];
}

#pragma mark -- MFMailComposure Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
