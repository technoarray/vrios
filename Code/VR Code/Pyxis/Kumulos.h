//
//  Kumulos.h
//  Kumulos
//
//  Created by Kumulos Bindings Compiler on Apr 23, 2017
//

#import <Foundation/Foundation.h>
@import KumulosSDK;

typedef Kumulos kumulosProxy;

@protocol KumulosDelegate <NSObject>
@optional

- (void) kumulosAPI:(kumulosProxy*)kumulos apiOperation:(KSAPIOperation*)operation didFailWithError:(NSString*)theError;

- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation deleteLoginDetailsDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation deleteUserWithIdentifierDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation getLoginDetailsDidCompleteWithResult:(NSArray*)theResults;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation saveLoginInfoDidCompleteWithResult:(NSNumber*)newRecordID;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation checkEmailExistDidCompleteWithResult:(NSArray*)theResults;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation loginUserDidCompleteWithResult:(NSArray*)theResults;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation signUpDidCompleteWithResult:(NSNumber*)newRecordID;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation updateCurrentStatusDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation updatePasswordDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation updateSchoolDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation updateSubscriptionStatusDidCompleteWithResult:(NSNumber*)affectedRows;
- (void) kumulosAPI:(Kumulos*)kumulos apiOperation:(KSAPIOperation*)operation userDetailDidCompleteWithResult:(NSArray*)theResults;

@end

@interface Kumulos (Bindings)

@property (nonatomic,weak) id <KumulosDelegate> delegate;

- (instancetype) init;

- (KSAPIOperation*) deleteLoginDetailsWithUser:(NSUInteger)user;
- (KSAPIOperation*) deleteUserWithIdentifierWithDeviceIdentifier:(NSString*)deviceIdentifier andUser:(NSUInteger)user;
- (KSAPIOperation*) getLoginDetailsWithUser:(NSUInteger)user;
- (KSAPIOperation*) saveLoginInfoWithDeviceIdentifier:(NSString*)deviceIdentifier andUser:(NSUInteger)user;
- (KSAPIOperation*) checkEmailExistWithEmailAddress:(NSString*)emailAddress;
- (KSAPIOperation*) loginUserWithEmailAddress:(NSString*)emailAddress andPassword:(NSString*)password;
- (KSAPIOperation*) signUpWithFirstName:(NSString*)firstName andLastName:(NSString*)lastName andEmailAddress:(NSString*)emailAddress andPassword:(NSString*)password andIsSubscribed:(BOOL)isSubscribed andCurrentStatus:(NSString*)currentStatus andSchool:(NSString*)school;
- (KSAPIOperation*) updateCurrentStatusWithCurrentStatus:(NSString*)currentStatus andUserID:(NSUInteger)userID;
- (KSAPIOperation*) updatePasswordWithPassword:(NSString*)password andUserID:(NSUInteger)userID andNewPassword:(NSString*)newPassword;
- (KSAPIOperation*) updateSchoolWithSchool:(NSString*)school andUserID:(NSUInteger)userID;
- (KSAPIOperation*) updateSubscriptionStatusWithIsSubscribed:(BOOL)isSubscribed andUserID:(NSUInteger)userID;
- (KSAPIOperation*) userDetailWithUserID:(NSUInteger)userID;

@end