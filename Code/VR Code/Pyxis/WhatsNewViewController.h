//
//  WhatsNewViewController.h
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "WebAPI.h"

@interface WhatsNewViewController : UIViewController<APIsDelegate>
{
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITextView *whatsNewTxt;
@property (strong, nonatomic) IBOutlet UIButton *videoList;
@property (strong, nonatomic) IBOutlet UITextView *aboutUsTxt;
- (IBAction)login:(id)sender;
- (IBAction)videoList:(id)sender;
@end
