//
//  ChoosePlanViewController.h
//  Pyxis
//
//  Created by Saurav on 02/03/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPViewController.h"
#import "SignUpViewController.h"

@interface ChoosePlanViewController : UIViewController

- (IBAction)MontlySubscription:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *YearlySubscription;
- (IBAction)back:(id)sender;
- (IBAction)YearlySubscription:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *message;
@end
