//
//  WebAPI.h
//  browze
//
//  Created by HashBrown Systems on 04/05/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "ConnectionHandler.h"
#import "Constants.h"
#import "WebAPI.h"

@protocol APIsDelegate <NSObject>

@optional
- (void) callbackFromAPI:(id)response;

//user methods
-(void) callBackSignupSuccess;
-(void) callBackCategoryListSuccess:(NSDictionary *)response;
-(void) callBackVideoListSuccess:(NSDictionary *)response;
-(void) callBackAppInfoSuccess:(NSDictionary *)response;
-(void) callBackForgotPasswordSuccess:(NSDictionary *)response;
-(void) callBackloginSuccess:(NSDictionary *)response;



@end

@interface WebAPI : NSObject
{
    __weak id <APIsDelegate> delegate;
}

@property (nonatomic,weak) id delegate;

//user methods
-(void)getCategoriesList;
-(void)signupUserWithDetails:(NSDictionary *)details;
-(void)getVideosList:(NSDictionary *)details;
-(void)loginUser:(NSString *)email password:(NSString *)pass;
-(void)updateUserSubscriptionStatus:(NSString *)status;
-(void)getAppInfo;
-(void)updatePassword:(NSString *)currentPassword newPassword:(NSString *)newPassword;
-(void)updateUserProfileWithType:(NSString *)type value:(NSString *)value;
-(void)forgotPassword:(NSString *)email;
@end
