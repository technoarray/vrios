//
//  SignUpViewController.m
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_isYearly==NO)
    {
        _subscriptionTxt.text=@"1 Month Subscription:";
        [_purchaseBtn setTitle:@"Purchase $15.99" forState:UIControlStateNormal];
    }
    
    schoolList=[Helper getSchoolList];
    
    currentStatusList = [Helper getCurrentStatusList];
    
    kumulos = [[Kumulos alloc]init];
    kumulos.delegate=self;
    
    api=[[WebAPI alloc]init];
    api.delegate=self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(agree) name:@"Agree" object:nil];
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkBox:(id)sender
{
    if (isChecked==NO)
    {
        isChecked=YES;
        [_checkBoxBtn setImage:[UIImage imageNamed:@"FilledCheck.png"] forState:UIControlStateNormal];
    }
    else
    {
        isChecked=NO;
        [_checkBoxBtn setImage:[UIImage imageNamed:@"EmptyCheck.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)purchase:(id)sender
{
    NSString *message=nil;
    if (_firstName.text.length==0)
    {
        message=@"Please enter your first name.";
    }
    else if (_lastName.text.length==0)
    {
        message=@"Please enter your last name.";
    }
    else if (_email.text.length==0)
    {
        message=@"Please enter your email.";
    }
    else if(![Helper validateEmailWithString:_email.text])
    {
        message=@"Please enter valid email.";
    }
    else if (_password.text.length==0)
    {
        message=@"Please enter password.";
    }
    else if (_confirm.text.length==0)
    {
        message=@"Please confirm your password.";
    }
    else if (![_confirm.text isEqualToString:_password.text])
    {
        message=@"Password does not match.";
    }
    else if (_currentStatus.text.length==0)
    {
        message=@"Please select current status.";
    }
    else if (_school.text.length==0)
    {
        message=@"Please select your school.";
    }
    else if (isChecked==NO)
    {
        message=@"Please accept conditions.";
    }
    else if ([Helper isInternetConnected]==NO)
    {
        message=INTERNET_ERROR;
    }
    
    if (message)
    {
        [Helper showAlertViewWithTitle:OOPS message:message];
    }
    else
    {
        [self hidekeyboard];
        
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsViewController"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

-(void)doneClicked
{
    _school.text=schoolName;
    [_school resignFirstResponder];
}

-(void)done
{
    _currentStatus.text=statusName;
    [_currentStatus resignFirstResponder];
}

-(void)agree
{
    [Helper showIndicatorWithText:@"Loading..." inView:self.view];
    [kumulos checkEmailExistWithEmailAddress:_email.text];
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(kumulosProxy *)kumulos apiOperation:(KSAPIOperation *)operation didFailWithError:(NSString *)theError
{
    NSLog(@"%@", theError);
    [Helper hideIndicatorFromView:self.view];
    [Helper showAlertViewWithTitle:ALERT message:@"Please try again."];
}

-(void)kumulosAPI:(Kumulos *)kumulos1 apiOperation:(KSAPIOperation *)operation checkEmailExistDidCompleteWithResult:(NSArray *)theResults
{
    NSLog(@"%@", theResults);
    if (theResults.count>0)
    {
        [Helper hideIndicatorFromView:self.view];
        [Helper showAlertViewWithTitle:ALERT message:@"Email address already exist."];
    }
    else
    {
        [kumulos signUpWithFirstName:_firstName.text andLastName:_lastName.text andEmailAddress:_email.text andPassword:_password.text andIsSubscribed:NO andCurrentStatus:statusName andSchool:schoolName];
    }
}

-(void)kumulosAPI:(Kumulos *)kumulos1 apiOperation:(KSAPIOperation *)operation signUpDidCompleteWithResult:(NSNumber *)newRecordID
{
    if ([newRecordID integerValue]>0)
    {
        [kumulos userDetailWithUserID:[newRecordID integerValue]];
    }
    else
    {
        [Helper hideIndicatorFromView:self.view];
        [Helper showAlertViewWithTitle:ALERT message:@"Please try again."];
    }
}

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation userDetailDidCompleteWithResult:(NSArray *)theResults
{
    [Helper hideIndicatorFromView:self.view];
    if (theResults.count>0)
    {
        NSDictionary *dict = [theResults firstObject];
        [Helper saveAllValuesToUserDefault:dict];
        
        [api signupUserWithDetails:dict];
        
        //perform in-app-purchase
        IAPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"IAPViewController"];
        controller.isYearly=_isYearly;
        [self.navigationController setViewControllers:@[controller] animated:YES];
    }
    else
    {
        [Helper showAlertViewWithTitle:ALERT message:@"Please try again."];
    }
}

#pragma mark -- APIs Delegate

-(void)callbackFromAPI:(id)response
{
    [Helper hideIndicatorFromView:self.view];
}

-(void)callBackSignupSuccess
{
    NSLog(@"data added to server");
}

#pragma mark -- Helper Methods

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)hidekeyboard
{
    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_email resignFirstResponder];
    [_password resignFirstResponder];
    [_currentStatus resignFirstResponder];
}

#pragma mark -- UiTextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag==1)
    {
        [_lastName becomeFirstResponder];
    }
    else if (textField.tag==2)
    {
        [_email becomeFirstResponder];
    }
    else if (textField.tag==3)
    {
        [_password becomeFirstResponder];
    }
    else if (textField.tag==4)
    {
        [_confirm becomeFirstResponder];
    }
    else if (textField.tag==5)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==6 || textField.tag==7)
    {
        UIPickerView *picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 200)];
        picker.delegate=self;
        picker.dataSource=self;
        picker.showsSelectionIndicator=YES;
        
        UIToolbar* toolbar = [[UIToolbar alloc] init];
        toolbar.frame=CGRectMake(0,0,self.view.frame.size.width,50);
        toolbar.barStyle = UIBarStyleBlackTranslucent;
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton;
        if (textField.tag==7)
        {
            schoolName = [schoolList firstObject];
            picker.tag=1;
            doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                          style:UIBarButtonItemStyleDone target:self
                                                         action:@selector(doneClicked)];
        }
        else
        {
            statusName=[currentStatusList firstObject];
            picker.tag=2;
            doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                          style:UIBarButtonItemStyleDone target:self
                                                         action:@selector(done)];
        }
        [textField setInputView:picker];
        
        [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
        
        textField.inputAccessoryView = toolbar;
    }
    return YES;
}

#pragma mark -- UIPickerView Delegate

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        return currentStatusList.count;
    }
    return schoolList.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==2)
    {
        return currentStatusList[row];
    }
    return schoolList[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    if (pickerView.tag==2)
    {
        statusName = currentStatusList[row];
        return;
    }
    schoolName=schoolList[row];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
