//
//  ChangePasswordViewController.h
//  Pyxis
//
//  Created by Saurav on 23/04/17.
//  Copyright © 2017 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "Kumulos.h"
#import "WebAPI.h"

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate, KumulosDelegate, APIsDelegate>
{
    UITextField *activeField;
    Kumulos *kumulos;
    WebAPI *api;
}

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *current;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirm;
- (IBAction)changePassword:(id)sender;
- (IBAction)back:(id)sender;
@end
