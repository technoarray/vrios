//
//  WelcomeViewController.h
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn;
@property (strong, nonatomic) IBOutlet UILabel *welcomeMessage;
- (IBAction)continue:(id)sender;
@end
