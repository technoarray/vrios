//
//  WelcomeViewController.m
//  Pyxis
//
//  Created by Saurav on 19/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.view.frame.size.height==480)
    {
        _logoWidth.constant=80;
    }
    else if (self.view.frame.size.height==568)
    {
        _logoWidth.constant=100;
    }
    
    _continueBtn.layer.cornerRadius=5;
    _continueBtn.layer.shadowColor=[UIColor blackColor].CGColor;
    _continueBtn.layer.shadowRadius=2;
    _continueBtn.layer.shadowOffset=CGSizeMake(-2, 2);
    _continueBtn.layer.shadowOpacity=0.5;
    
    _welcomeMessage.text=@"Welcome to \nEXERO VET \nA VR Surgical Database";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)continue:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WhatsNewViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
