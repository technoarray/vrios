//
//  Helper.h
//  browze
//
//  Created by HashBrown Systems on 26/03/15.
//  Copyright (c) 2015 Hashbrown Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "Constants.h"

@interface Helper : NSObject<UIAlertViewDelegate>

+(BOOL)isLocationServiceEnabled;
+(BOOL) isInternetConnected;
+(NSString *)calculatePlainTextTimeFromSeconds:(float)seconds;
+(NSString *)getNotificationToken;
+(NSString *)getIMEINumber;
+(NSString *)getApplicationVersion;
+(NSNumber *)getNumberFromString:(NSString *)strValue;
+(NSArray *)getSchoolList;
+(NSArray *)getCurrentStatusList;

+(BOOL)validateEmailWithString:(NSString*)emailmatch;
+(BOOL)validateUsernameWithString:(NSString*)username;

+(void)saveAllValuesToUserDefault:(NSDictionary *)completed;

+(CGRect)calculateHeightForText:(NSString *)text fontName:(NSString *)fontName fontSize:(float)fontSize maximumWidth:(float)width;
+(CGRect)calculateHeightForAttributedText:(NSAttributedString *)text font:(UIFont *)font maximumWidth:(float)width;

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;

+ (UIColor *) colorFromHexString:(NSString *)hexString;
+ (NSString *)getDateStringFromDate:(NSDate *)date;
+ (NSDate *)getDateFromDateString:(NSString *)dateStr;
+(BOOL)removeAllKeysFromUserDefaults;

// indicator
+(void)showIndicatorWithText:(NSString *)text inView:(UIView *)view;
+(void)hideIndicatorFromView:(UIView *)view;

@end
