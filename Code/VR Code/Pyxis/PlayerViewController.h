//
//  PlayerViewController.h
//  Pyxis
//
//  Created by Saurav on 30/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import <JWPlayerVRSDK/JWPlayerVRSDK.h>
@interface PlayerViewController : UIViewController<JWVrVideoViewDelegate>
{
    BOOL isPlaying;
    float totalDuration, playingTime;
    JWVrVideoView *_player;
    
 }

@property (strong, nonatomic) NSDictionary *selectedVideo;
 
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UIButton *playPauseBtn;
@property (strong, nonatomic) IBOutlet UIView *seekBarView;
@property (strong, nonatomic) IBOutlet UILabel *startTime;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UILabel *totalTime;
 

//@property (weak, nonatomic) IBOutlet JWVrVideoView *player;


- (IBAction)PlayPause:(id)sender;
- (IBAction)sliderPositionChanged:(id)sender;
- (IBAction)back:(id)sender;
@end
