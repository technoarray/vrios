//
//  LogoutViewController.m
//  Pyxis
//
//  Created by Saurav on 21/12/16.
//  Copyright © 2016 Saurav. All rights reserved.
//

#import "LogoutViewController.h"

@interface LogoutViewController ()

@end

@implementation LogoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    kumulos=[[Kumulos alloc]init];
    kumulos.delegate=self;
    
}

#pragma mark -- Button Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signOut:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sign Out" message:@"Do you really want to signout?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *logOut = [UIAlertAction actionWithTitle:@"Sign Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        [Helper showIndicatorWithText:@"Signing out..." inView:self.view];
        [kumulos deleteUserWithIdentifierWithDeviceIdentifier:[Helper getIMEINumber] andUser:[[[NSUserDefaults standardUserDefaults] objectForKey:@"MyId"] integerValue]];
    }];
    [alert addAction:cancel];
    [alert addAction:logOut];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)changePassword:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- Kumulos Delegate

-(void)kumulosAPI:(kumulosProxy *)kumulos apiOperation:(KSAPIOperation *)operation didFailWithError:(NSString *)theError
{
    NSLog(@"%@", theError);
    [Helper hideIndicatorFromView:self.view];
}

#pragma mark -- KuMuloas Delegate

-(void)kumulosAPI:(Kumulos *)kumulos apiOperation:(KSAPIOperation *)operation deleteUserWithIdentifierDidCompleteWithResult:(NSNumber *)affectedRows
{
    [Helper removeAllKeysFromUserDefaults];
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController setViewControllers:@[controller] animated:YES];
}

#pragma mark -- Memory Warning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
