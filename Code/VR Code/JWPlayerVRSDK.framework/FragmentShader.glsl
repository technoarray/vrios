precision mediump float;
varying vec2 v_TextureCoord;
uniform sampler2D s_Texture;

void main() {
    vec2 v_TextureCoordFixed = v_TextureCoord;
    // We need to correct out of range s and t coordinates here
    v_TextureCoordFixed = mod(v_TextureCoordFixed, vec2(1.0, 1.0));
    gl_FragColor = texture2D(s_Texture, v_TextureCoordFixed);
}