//
//  JWVrVideoViewController.h
//  JWPlayer-VR
//
//  Created by Rik Heijdens on 6/29/16.
//  Copyright © 2016 JW Player. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JWVrVideoViewDelegate.h"

#define STATE_PLAYING @"playing"
#define STATE_COMPLETE @"complete"
#define STATE_IDLE @"idle"
#define STATE_BUFFERING @"buffering"
#define STATE_PAUSED @"paused"

#define SDK_VERSION @"0.1.2"

/**
 * The supported video layouts.
 */
typedef NS_ENUM(NSInteger, StereoMode) {
    /**
     * Monoscopic video.
     */
    STEREO_MODE_MONO,
    
    /**
     * Stereoscopic video: Top half of the video contains the left eye 
     * and the bottom half contains the right eye.
     */
    STEREO_MODE_TOP_BOTTOM,
    
    /**
     * Stereoscopic video: Left half of the video contains the left eye
     * and the right half contains the right eye.
     */
    STEREO_MODE_LEFT_RIGHT
};

@import CoreMedia;

@interface JWVrVideoView : UIView

/* General Properties */

/**
 * The delegate receiving callbacks.
 * Internally the JWVrVideoView keeps a weak reference to the delegate.
 */
@property (nonatomic) id<JWVrVideoViewDelegate> delegate;

/**
 * Whether controls are enabled.
 */
@property (nonatomic) BOOL controls;

/**
 * Whether VR Mode is enabled.
 */
@property (nonatomic) BOOL vrModeEnabled;

/* Initializers */

- (instancetype)initWithURL:(NSString *)file
                 stereoMode:(StereoMode)stereoMode;

- (instancetype)initWithURL:(NSString *)file
                 stereoMode:(StereoMode)stereoMode
                   delegate:(id<JWVrVideoViewDelegate>)delegate;

/* Playback */

/**
 * The player state.
 * Can be: idle, buffering, paused, playing, or completed.
 * KVO compliant.
 */
@property (nonatomic, readonly) NSString *playerState;

/**
 * The current item's playback position.
 * If the time is unknown, NaN is returned.
 * KVO compliant.
 */
@property (nonatomic, readonly) NSNumber *playbackPosition;

/**
 * The current item's duration.
 * If the duration is unknown, NaN is returned.
 * If the duration is infinite, infinity is returned.
 * KVO compliant.
 */
@property (nonatomic, readonly) NSNumber *duration;

/**
 * The currently loaded media url.
 */
@property (nonatomic, readonly) NSString *loadedMediaUrl;

/**
 * The stereo mode the player is currently rendering in.
 */
@property (nonatomic, readonly) StereoMode stereoMode;

/**
 * Starts playback.
 */
- (void)play;

/**
 * Pauses playback.
 */
- (void)pause;

/**
 * Stops playback and unloads the current media.
 */
- (void)stop;

/**
 * Seeks to the given position in seconds.
 */
- (void)seek:(NSUInteger)position;

/**
 * Loads the given media.
 */
- (void)load:(NSString *)file
  withStereoMode:(StereoMode)stereoMode;

/* Fullscreen */

/**
 * Whether the player is currently in fullscreen.
 */
@property (nonatomic, readonly) BOOL isInFullscreen;

/**
 * Toggles the fullscreen state.
 * Does nothing if the player is in VR mode.
 *
 * @param fullscreen whether fullscreen should be enabled or disabled.
 */
- (void)toggleFullscreen:(BOOL)fullscreen;

/**
 * Returns the SDK version.
 */
+ (NSString *)SDKVersion;

@end
