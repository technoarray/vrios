//
//  JW Player VR SDK.h
//  JW Player VR SDK
//
//  Created by Rik Heijdens on 6/28/16.
//  Copyright © 2016 JW Player. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JW Player VR SDK.
FOUNDATION_EXPORT double JW_Player_VR_SDKVersionNumber;

//! Project version string for JW Player VR SDK.
FOUNDATION_EXPORT const unsigned char JW_Player_VR_SDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JWPlayerVRSDK/PublicHeader.h>
#import <JWPlayerVRSDK/JWVrVideoView.h>
#import <JWPlayerVRSDK/JWVrVideoViewDelegate.h>