//
//  JWVrVideoViewDelegate.h
//  JWPlayer-VR
//
//  Created by Rik Heijdens on 6/29/16.
//  Copyright © 2016 JW Player. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * The JWVrVideoView protocol defines methods that a delegate of a
 * JWVrVideoView object can optionally implement to intervene when player callbacks are captured.
 */
@protocol JWVrVideoViewDelegate <NSObject>

@optional

/**
 * Called when the player transitions into the idle state.
 */
- (void)onIdle:(NSString *)oldState;

/**
 * Called when the player transitions into the buffer state.
 */
- (void)onBuffer:(NSString *)oldState;

/**
 * Called when the player transitions into the playing state.
 */
- (void)onPlay:(NSString *)oldState;

/**
 * Called when the player transitions into the paused state.
 */
- (void)onPause:(NSString *)oldState;

/**
 * Called when the player transitions into the completed state, 
 * happens when the current item completed playback.
 */
- (void)onComplete:(NSString *)oldState;

/**
 * Called when the VRMode has been toggled.
 */
// TODO: Rename onVRModeToggled?
- (void)onVRModeEnabled:(BOOL)enabled;

/**
 * Called when the Fullscreen state is toggled.
 */
- (void)onFullscreenToggled:(BOOL)fullscreen;

/**
 * Called when the Cardboard trigger is used.
 * Triggered by touches on the screen.
 */
- (void)onCardboardTrigger;

@end
